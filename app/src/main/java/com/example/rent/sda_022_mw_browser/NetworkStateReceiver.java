package com.example.rent.sda_022_mw_browser;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by RENT on 2017-05-10.
 */

public class NetworkStateReceiver extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {
        String message = "";

        if (isConnected(context)){
            message = "Polaczono z siecia";
        } else {
            message = "Rozlaczono";
        }

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private boolean isConnected(Context context) {

        // w aktywnosci main nie trzeba dawac contextu jako argument
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected()
                && (networkInfo.getType() == ConnectivityManager.TYPE_WIFI
                || networkInfo.getType() == ConnectivityManager.TYPE_WIFI);
    }



}
