package com.example.rent.sda_022_mw_browser;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.web_view)
    protected WebView webView;

    @BindView(R.id.button_left)
    protected  Button buttonLeft;

    @BindView(R.id.button_right)
    protected  Button buttonRight;

    @BindView(R.id.button_go)
    protected Button buttonGo;

    @BindView(R.id.edit_text_www)
    protected EditText editTextWWW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        WebViewClient webViewClient = new WebViewClient();
        webView.setWebViewClient(webViewClient);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

//        boolean canGoBack = webView.canGoBack();
//        boolean canGoForward = webView.canGoForward();




    }

    @OnClick (R.id.button_left)
    public void clickButtonLeft(View view){
        webView.goBack();
    }

    @OnClick (R.id.button_right)
    public void clickButtonRight(View view){
        webView.goForward();
    }

    @OnClick (R.id.button_go)
    public void clickButtonGo(View view){
        webView.loadUrl("http://" + editTextWWW.getText().toString());
    }

    @Override
    protected void onPause() {
        super.onPause();
        NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();
        unregisterReceiver(networkStateReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();
        registerReceiver(networkStateReceiver, intentFilter);



    }
}
